require 'test_helper'

class AdvertTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "Add comment" do
    comment = Comment.new(comment: "tekstas")
    comment.user(users(:david))
    assert_equal true, adverts(:three).add_comment(comment: "tekstas")
  end

  test "returns the correct discount" do
    assert_equal 9.0, adverts(:three).make_discount(10)
  end

  test "sorting algorythm" do
    assert_equal [adverts(:three), adverts(:one), adverts(:two)], Advert.get_queue
  end

  test "Add comment to advert" do
    assert_equal true, adverts(:three).add_comment('comment')
  end

  test "Disallow comment" do
    assert_equal false, adverts(:dis).add_comment('comment')
  end

  test "turn on comments" do
    assert_equal true, adverts(:dis).comment_allow(true)
  end

  test "turn of comments" do
    assert_equal false, adverts(:dis).comment_allow(false)
  end

  test "can add comments" do
    assert_equal "comment", adverts(:dis).can_add_comment("comment")
  end

  test "change expiration date with false" do
    assert_equal false, adverts(:dis).change_expiration_date(2)
  end

  test "change expiration date succ" do
    assert_equal true, adverts(:three).change_expiration_date(2)
  end

end
