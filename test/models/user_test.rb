require 'test_helper'
require 'simplecov'
SimpleCov.start

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "money add" do
    users(:david).debt(5)
    assert_equal 5, users(:david).money
  end

  test "adverts make" do
    @advert = users(:david).adverts.create(title: 'Pavadinimas')
    assert_equal 1, users(:david).adverts.count
  end

  test "user make" do
    assert_equal true,  User.create(name: "edvinas").save
    assert_equal false,  User.create(name: "edvinas").save
  end

  test "debt money false" do
    assert_equal false, users(:john).debt(100)
  end

  test "credit money" do
    assert_equal 110, users(:john).credit(100)
  end

  test "pay for advert" do
    assert_equal true, users(:john).pay_for_advert(adverts(:three))
  end

  test "pay for advert when have 1 adverts" do
    users(:john).adverts.create(title: 'Kazkas');
    assert_equal true, users(:john).pay_for_advert(adverts(:three))
  end

  test "pay for advert when don't have money" do
    users(:john).adverts.create(title: 'Kazkas');
    assert_equal false, users(:john).pay_for_advert(Advert.new(name: 'Telefono adv', price: 10000))
  end
end
