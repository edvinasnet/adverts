class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :curr_from
      t.string :curr_to
      t.decimal :ratio, precision: 5, scale: 2
      t.timestamps null: false
    end
  end
end
