class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :comment
      t.integer :user, index: true
      t.integer :advert_id
      t.integer :rating
      t.boolean :comment_allow
      t.timestamps null: false
    end
  end
end
