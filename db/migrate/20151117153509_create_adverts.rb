class CreateAdverts < ActiveRecord::Migration
  def change
    create_table :adverts do |t|
      t.string :title
      t.string :text
      t.belongs_to :user, index: true
      t.belongs_to :category, index: true
      t.decimal :price, :precision => 5, :scale => 2, :default => 0.00
      t.string :category
      t.integer :star
      t.datetime :date
      t.timestamps :created_at
      t.boolean :allow_comment
      t.timestamps null: false
    end
  end
end
