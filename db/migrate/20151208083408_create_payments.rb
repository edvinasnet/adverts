class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :type
      t.decimal :vat, :decimal, :precision => 5, :scale => 2, :default => 0.00
      t.decimal :money, :decimal, :precision => 5, :scale => 2, :default => 0.00
      t.timestamps null: false
    end
  end
end