# -*- encoding: utf-8 -*-
# stub: test-unit-rails 1.0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "test-unit-rails"
  s.version = "1.0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Kouhei Sutou"]
  s.date = "2014-09-07"
  s.description = "Rails supports Test::Unit bundled in Ruby 1.8 and MiniTest but doesn't support test-unit 2. Rails with test-unit 2 works but is not fully worked.\n"
  s.email = ["kou@clear-code.com"]
  s.homepage = "https://github.com/test-unit/test-unit-rails"
  s.licenses = ["LGPLv2 or later"]
  s.rubygems_version = "2.5.0"
  s.summary = "test-unit-rails is a Rails adapter for test-unit 3. You can use full test-unit 3 features, \"RR\":https://rubygems.org/gems/rr integration and \"Capybara\":https://rubygems.org/gems/capybara integration with test-unit-rails."

  s.installed_by_version = "2.5.0" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 0"])
      s.add_runtime_dependency(%q<test-unit-activesupport>, [">= 1.0.2"])
      s.add_runtime_dependency(%q<test-unit-notify>, [">= 0"])
      s.add_runtime_dependency(%q<test-unit-capybara>, [">= 0"])
      s.add_runtime_dependency(%q<test-unit-rr>, [">= 0"])
      s.add_development_dependency(%q<bundler>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<packnga>, [">= 0"])
      s.add_development_dependency(%q<RedCloth>, [">= 0"])
    else
      s.add_dependency(%q<rails>, [">= 0"])
      s.add_dependency(%q<test-unit-activesupport>, [">= 1.0.2"])
      s.add_dependency(%q<test-unit-notify>, [">= 0"])
      s.add_dependency(%q<test-unit-capybara>, [">= 0"])
      s.add_dependency(%q<test-unit-rr>, [">= 0"])
      s.add_dependency(%q<bundler>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<packnga>, [">= 0"])
      s.add_dependency(%q<RedCloth>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, [">= 0"])
    s.add_dependency(%q<test-unit-activesupport>, [">= 1.0.2"])
    s.add_dependency(%q<test-unit-notify>, [">= 0"])
    s.add_dependency(%q<test-unit-capybara>, [">= 0"])
    s.add_dependency(%q<test-unit-rr>, [">= 0"])
    s.add_dependency(%q<bundler>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<packnga>, [">= 0"])
    s.add_dependency(%q<RedCloth>, [">= 0"])
  end
end
