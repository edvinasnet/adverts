# -*- encoding: utf-8 -*-
# stub: test-unit-notify 1.0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "test-unit-notify"
  s.version = "1.0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Kouhei Sutou"]
  s.date = "2014-10-13"
  s.description = "A test result notify extension for test-unit.\nThis provides test result notification support.\n"
  s.email = ["kou@clear-code.com"]
  s.homepage = "https://github.com/test-unit/test-unit-notify"
  s.licenses = ["LGPLv2.1 or later"]
  s.rubygems_version = "2.5.0"
  s.summary = "A test result notify extension for test-unit."

  s.installed_by_version = "2.5.0" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<test-unit>, [">= 2.4.9"])
      s.add_development_dependency(%q<bundler>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<yard>, [">= 0"])
      s.add_development_dependency(%q<packnga>, [">= 0"])
      s.add_development_dependency(%q<kramdown>, [">= 0"])
    else
      s.add_dependency(%q<test-unit>, [">= 2.4.9"])
      s.add_dependency(%q<bundler>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<yard>, [">= 0"])
      s.add_dependency(%q<packnga>, [">= 0"])
      s.add_dependency(%q<kramdown>, [">= 0"])
    end
  else
    s.add_dependency(%q<test-unit>, [">= 2.4.9"])
    s.add_dependency(%q<bundler>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<yard>, [">= 0"])
    s.add_dependency(%q<packnga>, [">= 0"])
    s.add_dependency(%q<kramdown>, [">= 0"])
  end
end
