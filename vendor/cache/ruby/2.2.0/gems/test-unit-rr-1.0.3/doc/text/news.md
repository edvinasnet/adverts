# News

## 1.0.3: 2013-07-07

### Improvements

* Supported rr 1.1.1 or later. Older rr support is dropped.
  [Reported by Youhei SASAKI] [GitHub#2]

### Thanks

* Youhei SASAKI

## 1.0.2: 2012-11-04

### Improvements

* Added `assert_rr`.

### Fixes

* Fixed wrong license notation in source header.
  [Reported by mtasaka] [GitHub#1]

### Thanks

* mtasaka

## 1.0.1: 2011-02-09

### Improvements

* run setup for RR before test case's setup and teardown
  for RR after test cases' teardown.

## 1.0.0: 2011-02-09

The first release!!!
