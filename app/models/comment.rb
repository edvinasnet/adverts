#comment class
class Comment < ActiveRecord::Base
  has_one :user
  belongs_to :advert

  validates_associated :user

  def up_rating(value)
    self.rating += value
  end

  def down_rating(value)
    self.rating -= value
  end
end
