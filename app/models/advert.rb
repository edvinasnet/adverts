#advert model
class Advert < ActiveRecord::Base
  include Comparable
  belongs_to :user
  belongs_to :category
  has_many :comments

  def self.get_queue
    list = Advert.where('date > ?', Time.now).order(star: :desc)
  end

  def make_discount(percentage)
    self.price -= self.price * (percentage.to_f / 100)
  end

  def change_expiration_date(new_date)
    if date < Time.now
      return FALSE
    else
      self.date += (new_date.to_i * 24 * 60 * 60)
      true
    end
  end

  def self.create_and_send_confirmation(email)
    transaction do
      advert = create!(
        title: params[:title],
        text: params[:text]

      )
      advert
    end
  end

  def add_comment(comment, comment_user)
    if allow_comment
      self.user(comment_user)
      comments.create(comment: comment)
      true
    else
      false
    end
  end

  def allow_to_favourite
    if date > Time.now
      true
    else
      false
    end
  end

  def comment_allow(value)
    if value == FALSE
      self.allow_comment = comments.size == 0 ? value : allow_comment
    else
      self.allow_comment = value
    end
  end

  def can_add_comment(comment)
    if comments.empty?
      return FALSE if comments.any? { |com| com == comment.comment }
    end
    comment_allow(comment)
  end
end
