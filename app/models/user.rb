#users class
class User < ActiveRecord::Base
  has_many :adverts
  belongs_to :comment
  validates :name, presence: true
  validates_uniqueness_of :name
  has_many :payments, dependent: :destroy

  def debt(money)
    if money <= self.money
      self.money -= money
      true
    else
      false
    end
  end

  def credit(money)
    self.money += money
  end

  def pay_for_advert(advert)
    return true if adverts.count % 3 == 0
    if debt(advert.price)
      return true
    else
      false
    end
  end

  def add_money(money, curr, payment)
    if payment.allow
      @money += payment.pay(money, curr).to_f
    end
  end

  def send_money(sum, to_user)
    if self.debt(sum)
      to_user.credit(sum)
    else
      false
    end
    # debit = (UserPayment.new).pay(sum, 'EUR', self)
    # user.money += debit if debit != FALSE
  end
end
