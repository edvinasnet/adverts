#payment class
class Payment < ActiveRecord::Base
  has_one :user

  def pay(sum, curr)
    ratio = 1
    ratio = Currency.get_calculate_ratio(curr, 'EUR') if curr != 'EUR'
    (sum * ratio - self.vat).round(2)
  end

end
