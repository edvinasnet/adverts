#currency class
class Currency < ActiveRecord::Base
  def self.get_calculate_ratio(curr_from, curr_to)
    Currency.where(curr_from: curr_from.to_s).where(curr_to: curr_to.to_s).first.ratio
  end
end
