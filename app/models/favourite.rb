#favourite class
class Favourite < ActiveRecord::Base
  belongs_to :user
  belongs_to :advert

  def self.add_favourite(advert, user)
    if advert.allow_to_favourite
      favourite = Favourite.new(advert: advert, user: user)
      favourite.save
      true
    else
      false
    end
  end

end
