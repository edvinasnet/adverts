class ConfirmMail < ApplicationMail

  def self.send_confirmation!(advert, email)
    confirmation_request(advert, email).deliver_now!
  end

  def confirmation_request(advert, email)
    @advert = advert
    mail to: email.to_s, subject: "Your advert"
  end
end