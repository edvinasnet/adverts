class AdvertController < ApplicationController

  def index
    @adverts = Advert.all
  end

  def show
    @advert = Advert.find(params[:id])
  end

  def new
    @advert = Advert.new
  end

  def create
    Advert.create_and_send_confirmation(params[:email])

  end


  def update
    puts params[:id]
    @advert = Advert.find(params[:id])
    advert_params = {:title => params[:title]}
    if @advert.update(advert_params)
      redirect_to @advert
    else
      render "edit"
    end
  end
end
