describe "advert/show.html.erb" do
  it "displays all the advert" do
    assign(:advert,
        double("advert", title: "slicer", price: 10)
    )
    render
    expect(rendered).to match /slicer/
  end
end