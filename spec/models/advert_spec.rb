require 'rails_helper'

RSpec.describe Advert, type: :model do
  fixtures :adverts, :users

  describe '#new' do
    it 'add comments' do
      comment = Comment.new(comment: "tekstas")
      expect(adverts(:three).add_comment("tekstas", users(:david))).to eql true
    end

    it 'allow add comments' do
      expect(adverts(:three).add_comment('comment', users(:david))).to eq true
    end

    it 'can add comments' do
      expect(adverts(:dis).can_add_comment("comment")).to eq "comment"

    end

    it 'Disallow comment' do
      expect(adverts(:dis).add_comment('comment', users(:david))).to eq false
    end

    it 'turn on comments' do
      expect(adverts(:dis).comment_allow(true)).to eq true

    end

    it 'turn off comments' do
      expect(adverts(:dis).comment_allow(false)).to eq false
    end
  end

  describe '#discount' do
    it 'returns the correct discount' do
      expect(adverts(:three).make_discount(10)).to eql 9.0
    end
  end

  describe '#expiration' do
    it 'change expiration date with false' do
      expect(adverts(:dis).change_expiration_date(2)).to eql false
    end

    it 'change expiration date succ' do
      expect(adverts(:three).change_expiration_date(2)).to eql false
    end
  end



end
