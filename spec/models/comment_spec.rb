require 'simplecov'
SimpleCov.start

RSpec.describe Comment, type: :model do
  before :each do

  end
  fixtures :comments, :adverts, :users


  describe '#ratings' do
    context 'change ratings' do
      it 'up' do
        expect(comments(:one).up_rating(1)).to eq(1)
      end

      it 'down' do
        expect(comments(:one).down_rating(1)).to eq(-1)
      end
    end
  end

end