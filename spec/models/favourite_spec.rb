require 'simplecov'
SimpleCov.start

RSpec.describe Favourite, type: :model do
  fixtures :users, :adverts

  before :each do
    @user = users("david")
    @advert = adverts("one")
  end

  describe '#add' do
    context 'money' do
      it 'advert' do
        @advert.stub(:allow_to_favourite){true}
        expect(Favourite.add_favourite(@advert, @user)).to eq(true)
      end
    end
  end

end
