require 'rails_helper'

RSpec.describe Payment, type: :model do
  before :each do
    @payment = Payment.new
  end

  describe '#pay' do
    context 'pay for advert when have 1 adverts' do

      it 'payment' do
        @payment.stub(:vat){1}
        expect(@payment.pay(2, 'EUR')).to eq(1)
      end
      end
  end
end
