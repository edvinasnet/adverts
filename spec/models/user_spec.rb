require 'simplecov'
SimpleCov.start

describe 'User object' do
  before :each do
    @user = double("user", credit: true)
  end
  fixtures :users, :adverts


  describe '#money' do
    context 'add money' do
      it 'has no skelbimai' do
        users(:david).debt(5)
        expect(users(:david).money).to eq(5.0)
      end
    end
    context 'debt money' do
      it 'debt more money' do
        expect(users(:john).debt(100)).to be false
      end
    end

    context 'send money ' do
      it 'to user' do
        expect(users(:john).send_money(10, @user)).to be true
      end
    end

    context 'send money ' do
      it 'to user' do
        expect(users(:john).send_money(20, @user)).to be false
      end
    end
  end

  describe '#adverts' do
    context 'make' do
      it 'advert' do
        @advert = users(:david).adverts.create(title: 'Pavadinimas')
        expect(users(:david).adverts.count).to be 1
      end
    end
  end

  describe '#user make' do
    context 'make' do
      it 'advert' do
        expect(User.create(name: "edvinas").save).to be true
        expect(User.create(name: "edvinas").save).to be false
      end
    end
  end

  describe '#credit' do
    context 'money' do
      it 'advert' do
        expect(users(:john).credit(100)).to eq(110.0)
      end
    end
  end

  describe '#paying' do
    context 'pay for advert' do
      it 'advert' do
        expect(users(:john).pay_for_advert(adverts(:three))).to be true
      end
    end
    context 'pay for advert when have 1 adverts' do
      it 'advert' do
        users(:john).adverts.create(title: 'Kazkas');
        adverts(:three).stub(:price){30}
        expect(users(:john).pay_for_advert(adverts(:three))).to be false
      end
    end

    context 'return false for adverts' do
      it 'advert' do
        users(:john).adverts.create(title: 'Kazkas');

        expect(users(:john).pay_for_advert(adverts(:three))).to be true
      end
    end


  end
end
