require 'rails_helper'

RSpec.describe Currency, type: :model do
  fixtures :currencies

  describe '#discount' do
    it 'returns the correct discount' do
      expect(Currency.get_calculate_ratio('EUR', 'GBP')).to eql 0.75
    end
  end

end
