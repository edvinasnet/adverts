require 'rails_helper'

RSpec.describe AdvertController, type: :controller do
  describe "Edit advert" do
    fixtures :adverts
    it "Edit advert" do
      post :update, {:id => 1, :title => 'new content'}
      expect(response).to redirect_to(adverts(:one))
    end
  end

end
