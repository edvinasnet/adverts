require 'rails_helper'

RSpec.describe UserController, type: :controller do

  describe "GET index" do
    it "assigns @users" do
      user = User.create
      get :new
      expect(response).to render_template(:new)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end


    it "creates a user and redirects to the user's page" do
      get :create
      expect(response).to render_template(:new)
      #follow_redirect!


    end
    it "Create db object" do
      post :create, {:name => "Edvinas"}
      #expect(response).to redirect_to(:index)
      #expect(response).to render_template(:index)
      expect(response.body).to include("redirected")
    end

  end
end
